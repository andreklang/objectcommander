package primitives

import (
    "testing"
    "fmt"
)

func TestPosition_String(t *testing.T) {
    position := Position{
        X:1,
        Y:2,
    }

    if fmt.Sprintf("%s", position) != "1,2" {
        t.Errorf("Wrong conversion to string, expected 1,2, got %s", position)
    }
}
