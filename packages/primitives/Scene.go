package primitives

type Scene struct {
    Width int64
    Height int64
}

// IsPositionValid checks if the given position is within the boundaries
// of this scene.
func (s *Scene) IsPositionValid(position *Position) (result bool) {

    // Positions cant be negative
    if position.X < 0 || position.Y < 0 {
        return
    }

    if position.X > (s.Width - 1) {
        return
    }

    if position.Y > (s.Height - 1) {
        return
    }

    // if we got here, we are within limits
    result = true

    return
}