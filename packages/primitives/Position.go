package primitives

import "fmt"

type Position struct {
    X int64
    Y int64
}

// String converts the position to a standardized X,Y string.
// Mostly for use in messages, unit-tests & debugging
func (p Position) String() string {
    return fmt.Sprintf("%d,%d", p.X, p.Y)
}