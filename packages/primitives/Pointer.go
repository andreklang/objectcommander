package primitives

import "fmt"

type Pointer struct {
    Scene           *Scene
    CurrentHeading  Heading
    CurrentPosition Position
}

// RotateCW rotates the pointers current heading one step ClockWise.
// For example from North to East
func (p *Pointer) RotateCW() {
    if p.CurrentHeading == HEADING_WEST {
        p.CurrentHeading = HEADING_NORTH
        return
    }

    p.CurrentHeading++
}

// RotateCCW rotates the pointers current heading one step CounterClockWise.
// For example from North to West
func (p *Pointer) RotateCCW() {
    if p.CurrentHeading == HEADING_NORTH {
        p.CurrentHeading = HEADING_WEST
        return
    }

    p.CurrentHeading--
}

// MoveForward moves the pointers position one step in the direction of
// CurrentHeading as long as the new position is valid within the Scene
func (p *Pointer) MoveForward() (err error) {

    // make a new position based on current Position
    newPos := Position{
        X: p.CurrentPosition.X,
        Y: p.CurrentPosition.Y,
    }

    switch p.CurrentHeading {
    case HEADING_NORTH:
        newPos.Y--
        break
    case HEADING_EAST:
        newPos.X++
        break
    case HEADING_SOUTH:
        newPos.Y++
        break
    case HEADING_WEST:
        newPos.X--
        break
    }

    // check if new position is valid in scene
    if !p.Scene.IsPositionValid(&newPos) {
        err = fmt.Errorf("cannot move to position %s since it is not valid in scene", newPos)
        return
    }

    // update current position
    p.CurrentPosition = newPos

    return
}

// MoveBackward moves the pointers position one step backwards from the direction of
// CurrentHeading as long as the new position is valid within the Scene
func (p *Pointer) MoveBackward() (err error) {

    // make a new position based on current Position
    newPos := Position{
        X: p.CurrentPosition.X,
        Y: p.CurrentPosition.Y,
    }

    switch p.CurrentHeading {
    case HEADING_NORTH:
        newPos.Y++
        break
    case HEADING_EAST:
        newPos.X--
        break
    case HEADING_SOUTH:
        newPos.Y--
        break
    case HEADING_WEST:
        newPos.X++
        break
    }

    // check if new position is valid in scene
    if !p.Scene.IsPositionValid(&newPos) {
        err = fmt.Errorf("cannot move to position %s since it is not valid in scene", newPos)
        return
    }

    // update current position
    p.CurrentPosition = newPos

    return
}
