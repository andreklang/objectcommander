package primitives

import "bitbucket.org/andreklang/objectCommander/internal/protocolreader"

type State struct {
    Pointer *Pointer
    Scene *Scene
    Alive bool
    Commands []string
}

func (s *State) Kill() {
    s.Alive = false
    s.Pointer.CurrentPosition.X = -1
    s.Pointer.CurrentPosition.Y = -1
    s.Commands = append(s.Commands, protocolreader.COMMAND_STOP)
}

func (s *State) Reset() {
    s.Alive = true
    s.Pointer.CurrentPosition.X = 0
    s.Pointer.CurrentPosition.Y = 0
    s.Commands = make([]string, 0)
}

func (s *State) MoveForward() {
    if !s.Alive {
        return
    }

    s.Commands = append(s.Commands, protocolreader.COMMAND_FORWARD)

    err := s.Pointer.MoveForward()
    if err != nil {
        s.Kill()
    }
}

func (s *State) MoveBackward() {
    if !s.Alive {
        return
    }

    s.Commands = append(s.Commands, protocolreader.COMMAND_BACKWARD)

    err := s.Pointer.MoveBackward()
    if err != nil {
        s.Kill()
    }
}

func (s *State) RotateCW() {
    if !s.Alive {
        return
    }

    s.Commands = append(s.Commands, protocolreader.COMMAND_CW)

    s.Pointer.RotateCW()
}

func (s *State) RotateCCW() {
    if !s.Alive {
        return
    }

    s.Commands = append(s.Commands, protocolreader.COMMAND_CCW)

    s.Pointer.RotateCCW()
}