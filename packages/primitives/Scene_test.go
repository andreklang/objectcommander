package primitives

import "testing"

func TestScene_IsPositionValid(t *testing.T) {

    // get a Scene that is 9 squares big
    scene := Scene{
        Width: 3,
        Height: 3,
    }

    Data := []struct{
        Pos Position
        Result bool
    }{
        {
            Pos: Position{
                X: 0,
                Y: 0,
            },
            Result: true,
        },
        {
            Pos: Position{
                X: 1,
                Y: 0,
            },
            Result: true,
        },
        {
            Pos: Position{
                X: 2,
                Y: 0,
            },
            Result: true,
        },
        {
            Pos: Position{
                X: 0,
                Y: 1,
            },
            Result: true,
        },
        {
            Pos: Position{
                X: 1,
                Y: 1,
            },
            Result: true,
        },
        {
            Pos: Position{
                X: 2,
                Y: 1,
            },
            Result: true,
        },
        {
            Pos: Position{
                X: 0,
                Y: 2,
            },
            Result: true,
        },
        {
            Pos: Position{
                X: 1,
                Y: 2,
            },
            Result: true,
        },
        {
            Pos: Position{
                X: 2,
                Y: 2,
            },
            Result: true,
        },
        {
            Pos: Position{
                X: -1,
                Y: 0,
            },
            Result: false,
        },
        {
            Pos: Position{
                X: 0,
                Y: -1,
            },
            Result: false,
        },
        {
            Pos: Position{
                X: -1,
                Y: -1,
            },
            Result: false,
        },
        {
            Pos: Position{
                X: 3,
                Y: 0,
            },
            Result: false,
        },
        {
            Pos: Position{
                X: 0,
                Y: 3,
            },
            Result: false,
        },
        {
            Pos: Position{
                X: 3,
                Y: 3,
            },
            Result: false,
        },
        {
            Pos: Position{
                X: -1,
                Y: 3,
            },
            Result: false,
        },
        {
            Pos: Position{
                X: 3,
                Y: -1,
            },
            Result: false,
        },
    }

    for _, test := range Data {

        if scene.IsPositionValid(&test.Pos) != test.Result {
            if test.Result {
                t.Errorf("Position %d,%d should be valid", test.Pos.X, test.Pos.Y)
            } else {
                t.Errorf("Position %d,%d should not be valid", test.Pos.X, test.Pos.Y)
            }
        }

    }


}
