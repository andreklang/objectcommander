package primitives

const HEADING_NORTH = 0
const HEADING_EAST = 1
const HEADING_SOUTH = 2
const HEADING_WEST = 3

type Heading int64

// String converts the position to a standardized X,Y string.
// Mostly for use in messages, unit-tests & debugging
func (p Heading) String() string {
    switch p {
    case HEADING_NORTH:
        return "↑"
    case HEADING_EAST:
        return "→"
    case HEADING_SOUTH:
        return "↓"
    case HEADING_WEST:
        return "←"
    default:
        return "¤"
    }
}