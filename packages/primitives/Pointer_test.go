package primitives

import "testing"

func TestPointer_RotateCW(t *testing.T) {
    pointer := Pointer{}

    // when initiated the pointer should point north
    if pointer.CurrentHeading != HEADING_NORTH {
        t.Errorf("Wrong initial heading")
    }

    // one step CW should be east
    pointer.RotateCW()
    if pointer.CurrentHeading != HEADING_EAST {
        t.Errorf("Wrong heading, should be east")
    }

    // another step CW should be south
    pointer.RotateCW()
    if pointer.CurrentHeading != HEADING_SOUTH {
        t.Errorf("Wrong heading, should be south")
    }

    // another step CW should be west
    pointer.RotateCW()
    if pointer.CurrentHeading != HEADING_WEST {
        t.Errorf("Wrong heading, should be west")
    }

    // Last step CW should be north
    pointer.RotateCW()
    if pointer.CurrentHeading != HEADING_NORTH {
        t.Errorf("Wrong heading, should be north")
    }
}

func TestPointer_RotateCCW(t *testing.T) {
    pointer := Pointer{}

    // when initiated the pointer should point north
    if pointer.CurrentHeading != HEADING_NORTH {
        t.Errorf("Wrong initial heading")
    }

    // one step CCW should be west
    pointer.RotateCCW()
    if pointer.CurrentHeading != HEADING_WEST {
        t.Errorf("Wrong heading, should be west")
    }

    // another step CCW should be south
    pointer.RotateCCW()
    if pointer.CurrentHeading != HEADING_SOUTH {
        t.Errorf("Wrong heading, should be south")
    }

    // one step CCW should be east
    pointer.RotateCCW()
    if pointer.CurrentHeading != HEADING_EAST {
        t.Errorf("Wrong heading, should be east")
    }

    // Last step CCW should be north
    pointer.RotateCCW()
    if pointer.CurrentHeading != HEADING_NORTH {
        t.Errorf("Wrong heading, should be north")
    }
}

func TestPointer_MoveForward(t *testing.T) {

    scene := Scene{
        Width: 3,
        Height: 3,
    }
    pointer := Pointer{
        Scene: &scene,
    }

    // rotate to EAST
    pointer.RotateCW()

    // move to 1,0
    err := pointer.MoveForward()
    if err != nil {
        t.Errorf("Moving to position 1,0 should not generate an error")
    } else {
        if pointer.CurrentPosition.X != 1 || pointer.CurrentPosition.Y != 0 {
            t.Errorf("Expected position 1,0. got %s", pointer.CurrentPosition)
        }
    }

    // move to 2,0
    err = pointer.MoveForward()
    if err != nil {
        t.Errorf("Moving to position 2,0 should not generate an error")
    } else {
        if pointer.CurrentPosition.X != 2 || pointer.CurrentPosition.Y != 0 {
            t.Errorf("Expected position 2,0. got %s", pointer.CurrentPosition)
        }
    }

    // move to INVALID position 3,0
    err = pointer.MoveForward()
    if err == nil {
        t.Errorf("Moving to invalid position 3,0 should generate an error")
    }
    if pointer.CurrentPosition.X != 2 || pointer.CurrentPosition.Y != 0 {
        t.Errorf("Expected position 2,0. got %s", pointer.CurrentPosition)
    }

    // rotate south
    pointer.RotateCW()

    // move to 2,1
    err = pointer.MoveForward()
    if err != nil {
        t.Errorf("Moving to position 2,1 should not generate an error")
    } else {
        if pointer.CurrentPosition.X != 2 || pointer.CurrentPosition.Y != 1 {
            t.Errorf("Expected position 2,1. got %s", pointer.CurrentPosition)
        }
    }

    // rotate east
    pointer.RotateCW()

    // move to 1,1
    pointer.MoveForward()

    // rotate north
    pointer.RotateCW()

    // move to 1,0
    pointer.MoveForward()

    // current position should be 1,0
    if pointer.CurrentPosition.X != 1 || pointer.CurrentPosition.Y != 0 {
        t.Errorf("Expected position 1,0. got %s", pointer.CurrentPosition)
    }

}

func TestPointer_MoveBackward(t *testing.T) {

    scene := Scene{
        Width: 3,
        Height: 3,
    }
    pointer := Pointer{
        Scene: &scene,
    }

    // rotate to west
    pointer.RotateCCW()

    // move to 1,0
    err := pointer.MoveBackward()
    if err != nil {
        t.Errorf("Moving to position 1,0 should not generate an error")
    } else {
        if pointer.CurrentPosition.X != 1 || pointer.CurrentPosition.Y != 0 {
            t.Errorf("Expected position 1,0. got %s", pointer.CurrentPosition)
        }
    }

    // move to 2,0
    err = pointer.MoveBackward()
    if err != nil {
        t.Errorf("Moving to position 2,0 should not generate an error")
    } else {
        if pointer.CurrentPosition.X != 2 || pointer.CurrentPosition.Y != 0 {
            t.Errorf("Expected position 2,0. got %s", pointer.CurrentPosition)
        }
    }

    // move to INVALID position 3,0
    err = pointer.MoveBackward()
    if err == nil {
        t.Errorf("Moving to invalid position 3,0 should generate an error")
    }
    if pointer.CurrentPosition.X != 2 || pointer.CurrentPosition.Y != 0 {
        t.Errorf("Expected position 2,0. got %s", pointer.CurrentPosition)
    }

    // rotate North
    pointer.RotateCW()

    // move to 2,1
    err = pointer.MoveBackward()
    if err != nil {
        t.Errorf("Moving to position 2,1 should not generate an error")
    } else {
        if pointer.CurrentPosition.X != 2 || pointer.CurrentPosition.Y != 1 {
            t.Errorf("Expected position 2,1. got %s", pointer.CurrentPosition)
        }
    }

    // rotate East
    pointer.RotateCW()

    // move to 1,1
    pointer.MoveBackward()

    // rotate South
    pointer.RotateCW()

    // move to 1,0
    pointer.MoveBackward()

    // current position should be 1,0
    if pointer.CurrentPosition.X != 1 || pointer.CurrentPosition.Y != 0 {
        t.Errorf("Expected position 1,0. got %s", pointer.CurrentPosition)
    }

}