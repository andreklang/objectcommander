package scenegui

import (
    "github.com/jroimartin/gocui"
)

func GuiLayout(gui *gocui.Gui) error {

    maxX, maxY := gui.Size()

    // stats view
    if _, err := gui.SetView(StatsView, 0, 0, maxX/3, maxY-1); err != nil {
        if err != gocui.ErrUnknownView {
            return err
        }
    }

    // scene view
    if _, err := gui.SetView(SceneView, maxX/3 + 1, 0, maxX-1, maxY-1); err != nil {
        if err != gocui.ErrUnknownView {
            return err
        }
    }

    return nil
}