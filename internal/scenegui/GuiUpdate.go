package scenegui

import (
    "github.com/jroimartin/gocui"
    "fmt"
    "bitbucket.org/andreklang/objectCommander/packages/primitives"
)

func GuiUpdate(state *primitives.State, gui *gocui.Gui) {

    // update StatsView
    gui.Update(func(g *gocui.Gui) error {
        view, err := g.View(StatsView)
        if err != nil {
            return err
        }
        view.Clear()

        fmt.Fprintln(view, "Use arrow keys to move:")
        fmt.Fprintln(view, " - Right: Turn 90° Clockwise")
        fmt.Fprintln(view, " - Left:  Turn 90° Counter Clockwise")
        fmt.Fprintln(view, " - Up:    Move one step forward")
        fmt.Fprintln(view, " - Down:  Move one step backward")
        fmt.Fprintln(view, "Enter to start over")
        fmt.Fprintln(view, "Ctrl+C to quit")
        fmt.Fprintf(view, "Current position: %s\n", state.Pointer.CurrentPosition)
        return nil
    })

    // Update SceneView
    gui.Update(func(g *gocui.Gui) error {
        view, err := g.View(SceneView)
        if err != nil {
            return err
        }
        view.Clear()

        if !state.Alive {
            fmt.Fprintln(view, "You be dead, press enter to reset or Ctrl+C to quit")
            return nil
        }

        var x,y int64
        for y = 0; y < state.Pointer.Scene.Width; y++ {
            var row string
            for x= 0 ; x < state.Pointer.Scene.Width; x++ {
                if x == state.Pointer.CurrentPosition.X && y == state.Pointer.CurrentPosition.Y {
                    row = row+state.Pointer.CurrentHeading.String()
                } else {
                    row = row+" "
                }
            }
            fmt.Fprintln(view, row)
        }
        return nil
    })
}