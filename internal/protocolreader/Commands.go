package protocolreader

const (
    COMMAND_STOP = "0"
    COMMAND_FORWARD = "1"
    COMMAND_BACKWARD = "2"
    COMMAND_CW = "3"
    COMMAND_CCW = "4"
)