package protocolreader

type Header struct {
    Width int64
    Height int64
    StartX int64
    StartY int64
}

