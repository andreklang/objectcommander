package protocolreader

import "testing"

func TestGetHeaderParts(t *testing.T) {
    header, err := GetHeaderParts("1,2,3,4")
    if err != nil {
        t.Errorf("Should not generate error: %s", err)
    }
    if header.Width != 1 {
        t.Errorf("expeced width 1, got %d", header.Width)
    }
    if header.Height != 2 {
        t.Errorf("expeced height 2, got %d", header.Height)
    }
    if header.StartX != 3 {
        t.Errorf("expeced startX 3, got %d", header.StartX)
    }
    if header.StartY != 4 {
        t.Errorf("expeced startY 4, got %d", header.StartY)
    }

    header, err = GetHeaderParts("1,2,3,4,5")
    if err == nil {
        t.Errorf("Should generate error due to incorrect amount of params")
    }

    header, err = GetHeaderParts("1")
    if err == nil {
        t.Errorf("Should generate error due to incorrect amount of params")
    }
}
