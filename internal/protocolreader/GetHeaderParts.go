package protocolreader

import (
    "strings"
    "strconv"
    "fmt"
)

func GetHeaderParts(line string) (header Header, err error) {
    headerParts := strings.Split(line, ",")

    if len(headerParts) != 4 {
        err = fmt.Errorf("wrong amount of params in header")
        return
    }

    var width, height, startX, startY int64

    width, err = strconv.ParseInt(headerParts[0], 10, 64)
    if err != nil {
        err = fmt.Errorf("can't parse width from header")
        return
    }

    height, err = strconv.ParseInt(headerParts[1], 10, 64)
    if err != nil {
        err = fmt.Errorf("can't parse height from header")
        return
    }

    startX, err = strconv.ParseInt(headerParts[2], 10, 64)
    if err != nil {
        err = fmt.Errorf("can't parse startX from header")
        return
    }

    startY, err = strconv.ParseInt(headerParts[3], 10, 64)
    if err != nil {
        err = fmt.Errorf("can't parse startY from header")
        return
    }

    header = Header{
        Width:width,
        Height:height,
        StartX:startX,
        StartY:startY,
    }

    return
}
