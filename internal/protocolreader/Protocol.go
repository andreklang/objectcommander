package protocolreader

import (
    "strings"
    "bufio"
    "os"
)

type Protocol struct {
    Header chan Header
    Commands chan string
    Error chan error
}

// Init just prepares the protocol reader instance
func (p *Protocol) Init()  {
    p.Header = make(chan Header)
    p.Commands = make(chan string)
    p.Error = make(chan error)
}

// Read reads from Stdin and posts to chan's when possible
// todo: refacor so it is easier to make a unit-test
func (p *Protocol) Read() {

    // set up a stdin-scanner
    scanner := bufio.NewScanner(os.Stdin)

    // read header line
    // list of int, separated by ,
    // width,height,StartX,StartY
    scanner.Scan()
    if err := scanner.Err(); err != nil {
        p.Error <- err
        return
    }

    // get the parsed data from header
    header, err := GetHeaderParts(scanner.Text())
    if err != nil {
        p.Error <- err
        return
    }

    // post the header so it can be used
    p.Header <- header
    close(p.Header)

    // read all the other lines
    for scanner.Scan() {

        row := scanner.Text()

        if row == "" {
            continue
        }

        // handle each command in the line
        for _, command := range strings.Split(row, ",") {

            // post known commands
            switch command {
            case COMMAND_STOP:
                p.Commands <- command
                return
            case COMMAND_FORWARD:
                p.Commands <- command
            case COMMAND_BACKWARD:
                p.Commands <- command
            case COMMAND_CW:
                p.Commands <- command
            case COMMAND_CCW:
                p.Commands <- command
            }
        }
    }

    if err := scanner.Err(); err != nil {
        p.Error <- err
        return
    }

    p.Commands <- COMMAND_STOP
    return
}