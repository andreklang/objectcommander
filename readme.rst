===============
objectCommander
===============

(In lack of better name)

Is a collection of CLI-tools to simulate, visualize & create a set of instructions according to the
protocol below.

Please note, Bitbucket is not to kind on .rst files (like this one), so it is better to clone the
repo and read it locally.

Protocol
========

The protocol can be read from StdIn, via keybord input or read from file.

The input consists of two parts, a header and a body, separated by a \r\n.

Header
------

The header is four integer values, separated by a comma (,).

The values are (in order)

* Scene width (x value)
* Scene height (y value)
* X Starting position
* Y Starting position

A header of `4,4,1,1` renders a Scene that is four positions wide and high. With current position set
at 1,1 (x,y).

Top left position is 0,0, bottom right position is 3,3.

Body
----

The body consists of a set of commands as integers, separated by comma (,). There MAY be linebrakes
in the body. In that case there should not be a separating comma, only a linebrake.

The commands is as follows:

0
  Quit program and print current position to StdOut
1
  Move forwards one step
2
  Move backwards one step
3
  Rotate Clockwise 90 degrees (from north to east)
4
  Rotate Counter Clockwise 90 degrees (from north to west)

.. note:: When keyboard is used as input, it is mandatory to send a command 0 (quit) to end simulation.
        This is optional if the protocol is read from a file.

Protocol example
----------------


This example will result in a ending position of 0,1.

.. code::

   4,4,2,2
   1,4,1,3,2,3,2,4,1,0

This illustrates a multi-line protocol, and will have ending position 89,6.

.. code::

   170,14,0,0
   3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2
   4,1,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2
   2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4
   2,2,2,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,1,1
   1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,4,1,1,1,1,1,1
   1,1,1,1,4,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2
   2,2,2,2,2,2,2,2,2,2,2,3,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,2,2
   2,2,2,2,4,2,2,2,2,3,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,4
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,2,2,2,2,2,4,4,1,1,1,1
   4,4,4,4,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1
   3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,3
   3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,4,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,3,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,4,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,3,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2
   2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2
   2,2,2,4,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,4,1,1,4,4,1,1,1,1,1,3,1,1,1,1,1,1
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,4,1,1,1,1,1,1,0

These examples and a few more is available in the sample/ directory.

Failing result
--------------
If the commands entered results in an invalid position, i.e. tries to move outside the scene an ending position
of -1,-1 will be returned.

Application Entry points
========================

There is three parts of this application:

simulator
  The main simulator, takes input via StdIn and returns the ending position to StdOut.
game
  A visual representation of a Scene that fits your terminal, an arrow-pointer indicating direction that you can
  control with your arrow-keys. Prints a resulting protocol to stdout when session is ended.
visualizer
  Reads a protocol from stdin, just like *simulator*, but visualizes it jut like *game* by running ten commands
  per second.

Running unit tests
==================

When building with docker (see below) the unit tests is run from the Dockerfile.

You can manually run the unit tests accordingly:

.. code:: bash

   go test bitbucket.org/andreklang/objectCommander/packages/primitives
   go test bitbucket.org/andreklang/objectCommander/internal/protocolreader

Building the applications
=========================

You can build the executables with docker, or with a local golang installation.

With Docker
-----------

Make sure to have docker installed and working on your local machine.

Run this in the current directory:

.. code:: bash

   id=$(docker create $(docker build -q .))
   docker cp $id:/build/simulator .
   docker cp $id:/build/game .
   docker cp $id:/build/visualizer .
   docker rm -v $id

This will give you all executables in the current directory.

.. note:: This will give you executables that will run in a Linux environment.

.. note:: Unit tests is run before the build, if any test fails the above code-block won't work.

With go
-------

If you have a working installation of go you can run the below in the current directory.

.. code:: bash

   cd cmd/simulator
   go build -o ../../simulator

   cd ../game
   go build -o ../../game

   cd ../visualizer
   go build -o ../../visualizer
   cd ../..

This will give you all executables in the current directory.

.. note:: This will give you executables that will run in your environment.

Running the applications
========================

After the applications are built you can run them accordingly:

.. code:: bash

   ./simulator < sample/0-1.1.simulation # will print 0,1

   ./simulator < sample/dead.simulation # will print -1,-1

   ./simulator # type the header, press enter, type the body, press enter. Will print result when command
               # 0 is given.

.. code:: bash

   ./game # will start a game-ui in the terminal, see additional information in ui for keymap
          # ctrl + c will stop application and print results

.. code:: bash

   ./visualizer < sample/89-6.simulation # will run a long visualization

Last notes
==========

Only the *simulator* was part of the assignment, the game and visualizer was just me getting carried away,
wanting to show how the *primitives* package could be used in more than one way, and it gave me an excuse
to play a little more with Go.

With that said, in those two applications there is much room for improvement. But it forced me to refactor
some most of the simulator to use common methods and packages.

These applications has only been tested on Linux.