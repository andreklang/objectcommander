FROM golang:alpine AS build-env

ENV GOPATH /build

ADD . /build/src/bitbucket.org/andreklang/objectCommander

RUN go test bitbucket.org/andreklang/objectCommander/internal/protocolreader
RUN go test bitbucket.org/andreklang/objectCommander/packages/primitives

RUN cd /build/src/bitbucket.org/andreklang/objectCommander/cmd/simulator \
  && go build -i -o /build/simulator

RUN cd /build/src/bitbucket.org/andreklang/objectCommander/cmd/game \
  && go build -i -o /build/game

RUN cd /build/src/bitbucket.org/andreklang/objectCommander/cmd/visualizer \
  && go build -i -o /build/visualizer