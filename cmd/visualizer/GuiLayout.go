package main

import (
    "github.com/jroimartin/gocui"
    "bitbucket.org/andreklang/objectCommander/internal/scenegui"
)

func GuiLayout(gui *gocui.Gui) error {

    maxX, maxY := gui.Size()

    // stats view
    if _, err := gui.SetView(scenegui.StatsView, 0, 0, maxX/3, maxY-1); err != nil {
        if err != gocui.ErrUnknownView {
            return err
        }
    }



    // scene view
    if _, err := gui.SetView(scenegui.SceneView, maxX/3 + 1, 0, maxX/3 + 3 + int(state.Scene.Width), 1 + int(state.Scene.Height)); err != nil {
        if err != gocui.ErrUnknownView {
            return err
        }
    }

    return nil
}