package main

import (
    "bitbucket.org/andreklang/objectCommander/packages/primitives"
    "github.com/jroimartin/gocui"
    "log"
    "bitbucket.org/andreklang/objectCommander/internal/scenegui"
    "bitbucket.org/andreklang/objectCommander/internal/protocolreader"
    "time"
)

var (
    state *primitives.State
)

func main() {

    // initiate a protocol reader
    protocol := protocolreader.Protocol{}
    protocol.Init()

    // start reading the protocol
    go func() {
        protocol.Read()
    }()

    // check for protocol errors
    go func() {
        err := <- protocol.Error
        log.Fatal(err)
    }()

    // Wait for the header
    header := <-protocol.Header

    // set up the scene
    scene := &primitives.Scene{
        Width: header.Width,
        Height: header.Height,
    }

    // set up pointer
    pointer := &primitives.Pointer{
        Scene: scene,
        CurrentPosition: primitives.Position{
            X: header.StartX,
            Y: header.StartY,
        },
    }

    // set up the state
    state = &primitives.State{
        Scene: scene,
        Pointer: pointer,
        Alive: true,
    }

    var gui *gocui.Gui
    var err error

    // setup gui
    gui, err = gocui.NewGui(gocui.OutputNormal)
    if err != nil {
        log.Panicln(err)
    }
    defer gui.Close()

    // connect the layout
    gui.SetManagerFunc(GuiLayout)


    // set up key bindings
    if err := guiKeyBindings(gui); err != nil {
        log.Panicln(err)
    }

    // start reading commands
    go func() {
        for {
            command := <-protocol.Commands

            // interpret commands
            switch command {
            case protocolreader.COMMAND_STOP:
                return
            case protocolreader.COMMAND_FORWARD:
                state.MoveForward()
            case protocolreader.COMMAND_BACKWARD:
                state.MoveBackward()
            case protocolreader.COMMAND_CW:
                state.RotateCW()
            case protocolreader.COMMAND_CCW:
                state.RotateCCW()
            }

            scenegui.GuiUpdate(state, gui)
            time.Sleep(100 * time.Millisecond)
        }
    }()

    // first render
    scenegui.GuiUpdate(state, gui)

    // start the gui
    // this is blocking until gui is closed
    if err := gui.MainLoop(); err != nil && err != gocui.ErrQuit {
        log.Panicln(err)
    }

}