package main

import (
    "github.com/jroimartin/gocui"
    "fmt"
    "os"
    "strings"
    "bitbucket.org/andreklang/objectCommander/internal/scenegui"
)

func guiKeyBindings(gui *gocui.Gui) error {
    if err := gui.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, closeUi); err != nil {
        return err
    }

    if err := gui.SetKeybinding("", gocui.KeyArrowUp, gocui.ModNone, arrowUp); err != nil {
        return err
    }

    if err := gui.SetKeybinding("", gocui.KeyArrowDown, gocui.ModNone, arrowDown); err != nil {
        return err
    }

    if err := gui.SetKeybinding("", gocui.KeyArrowRight, gocui.ModNone, arrowRight); err != nil {
        return err
    }

    if err := gui.SetKeybinding("", gocui.KeyArrowLeft, gocui.ModNone, arrowLeft); err != nil {
        return err
    }

    if err := gui.SetKeybinding("", gocui.KeyEnter, gocui.ModNone, restoreLife); err != nil {
        return err
    }

    return nil
}

func closeUi(gui *gocui.Gui, view *gocui.View) error {

    // add the ending command
    state.Commands = append(state.Commands, "0")

    // clear the screen
    gui.Close()

    // print results
    fmt.Printf("Ending Position: %s\n", state.Pointer.CurrentPosition)
    fmt.Printf("Simulator commands:\n\n")
    fmt.Printf("%d,%d,%d,%d\n", state.Pointer.Scene.Width, state.Pointer.Scene.Height, 0, 0)

    row := 0
    comCount := len(state.Commands)
    perRow := 30
    for {
        row++

        end := perRow * row
        if end > comCount {
            end = comCount
        }

        fmt.Println(strings.Join(state.Commands[(perRow * row)-perRow:end], ","))

        if row * perRow > comCount {
            break
        }
    }

    // exit
    os.Exit(0)

    return gocui.ErrQuit
}

func arrowUp(gui *gocui.Gui, view *gocui.View) error {

    state.MoveForward()

    scenegui.GuiUpdate(state, gui)
    return nil
}

func arrowDown(gui *gocui.Gui, view *gocui.View) error {

    state.MoveBackward()

    scenegui.GuiUpdate(state, gui)
    return nil
}

func arrowRight(gui *gocui.Gui, view *gocui.View) error {

    state.RotateCW()

    scenegui.GuiUpdate(state, gui)
    return nil
}

func arrowLeft(gui *gocui.Gui, view *gocui.View) error {

    state.RotateCCW()

    scenegui.GuiUpdate(state, gui)
    return nil
}

func restoreLife(gui *gocui.Gui, view *gocui.View) error {
    state.Reset()

    scenegui.GuiUpdate(state, gui)
    return nil
}