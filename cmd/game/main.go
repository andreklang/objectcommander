package main

import (
    "bitbucket.org/andreklang/objectCommander/packages/primitives"
    "github.com/jroimartin/gocui"
    "log"
    "bitbucket.org/andreklang/objectCommander/internal/scenegui"
)

var (
    state *primitives.State
)

func main() {

    var gui *gocui.Gui
    var err error

    // setup gui
    gui, err = gocui.NewGui(gocui.OutputNormal)
    if err != nil {
        log.Panicln(err)
    }
    defer gui.Close()

    // connect the layout
    gui.SetManagerFunc(scenegui.GuiLayout)

    // set up key bindings
    if err := guiKeyBindings(gui); err != nil {
        log.Panicln(err)
    }

    // get the size
    maxX, maxY := gui.Size()

    // set up the scene
    scene := &primitives.Scene{
        Width: int64(float64(maxX) * 0.66)-2,
        Height: int64(maxY)-2,
    }

    // set up pointer
    pointer := &primitives.Pointer{
        Scene: scene,
        CurrentPosition: primitives.Position{
            X: 0,
            Y: 0,
        },
    }

    // set up the state
    state = &primitives.State{
        Scene: scene,
        Pointer: pointer,
        Alive: true,
    }

    // first render
    scenegui.GuiUpdate(state, gui)

    // start the gui
    // this is blocking until gui is closed
    if err := gui.MainLoop(); err != nil && err != gocui.ErrQuit {
        log.Panicln(err)
    }

}