package main

import (
    "bitbucket.org/andreklang/objectCommander/packages/primitives"
    "bitbucket.org/andreklang/objectCommander/internal/protocolreader"
    "sync"
    "fmt"
    "log"
)

func main() {

    var wg sync.WaitGroup

    // initiate a protocol reader
    protocol := protocolreader.Protocol{}
    protocol.Init()

    // start reading the protocol
    wg.Add(1)
    go func() {
        protocol.Read()
        wg.Done()
    }()

    // check for protocol errors
    go func() {
        err := <- protocol.Error
        log.Fatal(err)
    }()

    // Wait for the header
    header := <-protocol.Header

    // set up the scene
    scene := &primitives.Scene{
        Width: header.Width,
        Height: header.Height,
    }

    // set up pointer
    pointer := &primitives.Pointer{
        Scene: scene,
        CurrentPosition: primitives.Position{
            X: header.StartX,
            Y: header.StartY,
        },
    }

    // set up the state
    state := &primitives.State{
        Scene: scene,
        Pointer: pointer,
        Alive: true,
    }

    // start reading commands
    wg.Add(1)
    go func() {
        defer wg.Done()
        for {
            command := <-protocol.Commands

            // interpret commands
            switch command {
            case protocolreader.COMMAND_STOP:
                return
            case protocolreader.COMMAND_FORWARD:
                state.MoveForward()
            case protocolreader.COMMAND_BACKWARD:
                state.MoveBackward()
            case protocolreader.COMMAND_CW:
                state.RotateCW()
            case protocolreader.COMMAND_CCW:
                state.RotateCCW()
            }

            // stop if a command above killed it
            if !state.Alive {
                wg.Done()
                return
            }
        }

        return
    }()

    // wait for all processes to finish
    wg.Wait()

    // return the position
    fmt.Println(state.Pointer.CurrentPosition.String())
}